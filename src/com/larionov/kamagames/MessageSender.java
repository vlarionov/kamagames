package com.larionov.kamagames;

public interface MessageSender {
    void send(long userId, String message);
}