package com.larionov.kamagames;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CachedPoolMessageSender implements MessageSender {
    private final Executor threadPool = Executors.newCachedThreadPool();

    @Override
    public void send(long userId, final String message) {
        List<MessageSenderUtils.Connection> connections = MessageSenderUtils.getUserConnections(userId);
        final CountDownLatch latch = new CountDownLatch(connections.size());

        for (final MessageSenderUtils.Connection connection : connections) {
            threadPool.execute(() -> {
                try {
                    MessageSenderUtils.send(connection, message);
                } catch (Exception e) {
                    log(e);
                } finally {
                    latch.countDown();
                }
            });
        }

        try {
            latch.await();
        } catch (InterruptedException ignored) {

        }
    }

    private void log(Exception e) {
        //log exception with appropriate level if necessary
    }
}
