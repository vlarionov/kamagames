package com.larionov.kamagames;

import java.util.ArrayList;
import java.util.List;

public final class MessageSenderUtils {
    /**
     * Thread-safe
     * Synchronous
     * Blocking
     * Calls network io to send message to client
     */
    public static void send(Connection connection, String message) throws InterruptedException {
        Thread.sleep(10);
    }

    public static List<Connection> getUserConnections(long userId) {
        return new ArrayList<>();
    }

    public final class Connection {

    }
}